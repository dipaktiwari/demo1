package com.example.demo1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo1.Entity.CollegeLockStatus;
import com.example.demo1.Entity.StandaloneLockStatus;
import com.example.demo1.Entity.UniversityLockStatus;
import com.example.demo1.bean.LockBean;
import com.example.demo1.repo.CollegeLockStatusRepo;
import com.example.demo1.repo.StandaloneLockStatusRepo;
import com.example.demo1.repo.UniversityLockStatusRepo;
import com.example.demo1.service.ServiceImpl;
@RestController
public class HomeController {
	  @Autowired ServiceImpl serviceImpl;
	  
	  @Autowired StandaloneLockStatusRepo standaloneLockStatusRepo;
	  @Autowired CollegeLockStatusRepo collegeLockStatusRepo;
	  @Autowired UniversityLockStatusRepo universityLockStatusRepo;
	 
	
	@PostMapping("/LockStatus")
	public ResponseEntity<?> createOrUpdateLockStatus(@RequestBody LockBean lockBean){
		String IntitutionType=lockBean.getIntitutionType();
		String aisheCode= lockBean.getAisheCode();
		Integer surveyYear=lockBean.getSurveyYear();
		String constant= lockBean.getConstant();
		Boolean status=lockBean.isStatus();
		
		if(aisheCode.equals(null) || aisheCode.equals("")) {
			return new ResponseEntity<String>("aisheCode can't be blank or null ", HttpStatus.NO_CONTENT);
		}
		if(surveyYear<1950 || surveyYear>2050) {
			return new ResponseEntity<String>("Survey Year can't be Blank or less than 1950 or greater than 2050",HttpStatus.NO_CONTENT);
		}
		if(IntitutionType.equalsIgnoreCase("S")) {
			StandaloneLockStatus entity=standaloneLockStatusRepo.findByAisheCodeAndSurveyYear(aisheCode, surveyYear);
			if(entity == null) {
				serviceImpl.saveStandalone(lockBean);
				return new ResponseEntity<String>("New Row Created",HttpStatus.CREATED);
			}
			else {
				serviceImpl.updateStandalone(lockBean, entity);
				return new ResponseEntity<String>("Status Updated",HttpStatus.OK);

			}
		}
		
		else if (IntitutionType.equalsIgnoreCase("c")) {
			CollegeLockStatus entity=collegeLockStatusRepo.findByAisheCodeAndSurveyYear(aisheCode, surveyYear);
			if(entity == null) {
				serviceImpl.saveCollege(lockBean);
				return new ResponseEntity<String>("New Row Created",HttpStatus.CREATED);

			}
			else {
				serviceImpl.updateCollege(lockBean, entity);
				return new ResponseEntity<String>("Status Updated",HttpStatus.OK);

			}
			
		}
		
		else if (IntitutionType.equalsIgnoreCase("u")) {
			UniversityLockStatus entity=universityLockStatusRepo.findByAisheCodeAndSurveyYear(aisheCode, surveyYear);
			if(entity == null) {
				serviceImpl.saveUniversity(lockBean);
				return new ResponseEntity<String>("New Row Created",HttpStatus.CREATED);

			}
			else {
				serviceImpl.updateUniversity(lockBean, entity);
				return new ResponseEntity<String>("Status Updated",HttpStatus.OK);
			}
		}
		return new ResponseEntity<String>("Intitution Type or constant not matched", HttpStatus.NOT_FOUND);
	}
}
