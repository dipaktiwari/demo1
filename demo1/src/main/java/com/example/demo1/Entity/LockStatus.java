package com.example.demo1.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="lock_status")
public class LockStatus {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long pk;
	
	@Column(name="intitution_type")
	private String intitutionType;
	
	@Column(name="aishe_code")
	private String aisheCode;
	
	@Column(name="survey_year")
	private int surveyYear;
	
	@Column(name="status")
	private boolean status;
	
	@Column(name="constant")
	private String constant;

	public String getIntitutionType() {
		return intitutionType;
	}

	public void setIntitutionType(String intitutionType) {
		this.intitutionType = intitutionType;
	}

	public String getAisheCode() {
		return aisheCode;
	}

	public void setAisheCode(String aisheCode) {
		this.aisheCode = aisheCode;
	}

	public int getSurveyYear() {
		return surveyYear;
	}

	public void setSurveyYear(int surveyYear) {
		this.surveyYear = surveyYear;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getConstant() {
		return constant;
	}

	public void setConstant(String constant) {
		this.constant = constant;
	}

	@Override
	public String toString() {
		return "LockStatus [intitutionType=" + intitutionType + ", aisheCode=" + aisheCode + ", surveyYear="
				+ surveyYear + ", status=" + status + ", constant=" + constant + "]";
	}

	public LockStatus(String intitutionType, String aisheCode, int surveyYear, boolean status, String constant) {
		super();
		this.intitutionType = intitutionType;
		this.aisheCode = aisheCode;
		this.surveyYear = surveyYear;
		this.status = status;
		this.constant = constant;
	}

	public LockStatus() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
