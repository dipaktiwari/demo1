package com.example.demo1.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="college_lock_status")
public class CollegeLockStatus {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int Id;
	
	@Column(name="aishe_code")
	private String aisheCode;
	
	@Column(name="survey_year")
	private int surveyYear;
	
	@Column(name="basic_detail")
	private Boolean basicDetail;
	
	@Column(name="address")
	private Boolean address;
	
	@Column(name="econtact")
	private Boolean econtact;
	
	@Column(name="vernacular_name")
	private Boolean vernacularName;
	
	@Column(name="alternate_name")
	private Boolean alternateName;
	
	@Column(name="list_of_department")
	private Boolean listOfDepartment;
	
	@Column(name="regular_prog_other")
	private Boolean regularProgOther;
	
	@Column(name="enroll_regular_prog_other")
	private Boolean enrollRegularProgOther;
	
	@Column(name="enroll_regular_foreign_student_through_other")
	private Boolean enrollRegularForeignStudentThroughOther;
	
	@Column(name="exam_result_regular_through_other")
	private Boolean examResultRegularThroughOther;
	
	@Column(name="teaching_staff")
	private Boolean teachingStaff;
	
	@Column(name="teaching_staff_econtact")
	private Boolean teachingStaffEcontact;
	
	@Column(name="teaching_staff_vernacular_name")
	private Boolean teachingStaffVernacularName;
	
	@Column(name="non_teaching_staff_detail")
	private Boolean nonTeachingStaffDetail;
	
	@Column(name="financial_info_income")
	private Boolean financialInfoIncome;
	
	@Column(name="financial_info_expenditure")
	private Boolean financialInfoExpenditure;
	
	@Column(name="infra")
	private Boolean infra;
	
	@Column(name="scholarship")
	private Boolean scholarship;
	
	@Column(name="fellowship")
	private Boolean fellowship;
	
	@Column(name="education_loan")
	private Boolean educationLoan;
	
	@Column(name="accreditation")
	private Boolean accreditation;
	
	@Column(name="final_lock")
	private Boolean finalLock;

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getAisheCode() {
		return aisheCode;
	}

	public void setAisheCode(String aisheCode) {
		this.aisheCode = aisheCode;
	}

	public int getSurveyYear() {
		return surveyYear;
	}

	public void setSurveyYear(int surveyYear) {
		this.surveyYear = surveyYear;
	}

	public Boolean isBasicDetail() {
		return basicDetail;
	}

	public void setBasicDetail(Boolean basicDetail) {
		this.basicDetail = basicDetail;
	}

	public Boolean isAddress() {
		return address;
	}

	public void setAddress(Boolean address) {
		this.address = address;
	}

	public Boolean isEcontact() {
		return econtact;
	}

	public void setEcontact(Boolean econtact) {
		this.econtact = econtact;
	}

	public Boolean isVernacularName() {
		return vernacularName;
	}

	public void setVernacularName(Boolean vernacularName) {
		this.vernacularName = vernacularName;
	}

	public Boolean isAlternateName() {
		return alternateName;
	}

	public void setAlternateName(Boolean alternateName) {
		this.alternateName = alternateName;
	}

	public Boolean isListOfDepartment() {
		return listOfDepartment;
	}

	public void setListOfDepartment(Boolean listOfDepartment) {
		this.listOfDepartment = listOfDepartment;
	}

	public Boolean isRegularProgOther() {
		return regularProgOther;
	}

	public void setRegularProgOther(Boolean regularProgOther) {
		this.regularProgOther = regularProgOther;
	}

	public Boolean isEnrollRegularProgOther() {
		return enrollRegularProgOther;
	}

	public void setEnrollRegularProgOther(Boolean enrollRegularProgOther) {
		this.enrollRegularProgOther = enrollRegularProgOther;
	}

	public Boolean isEnrollRegularForeignStudentThroughOther() {
		return enrollRegularForeignStudentThroughOther;
	}

	public void setEnrollRegularForeignStudentThroughOther(Boolean enrollRegularForeignStudentThroughOther) {
		this.enrollRegularForeignStudentThroughOther = enrollRegularForeignStudentThroughOther;
	}

	public Boolean isExamResultRegularThroughOther() {
		return examResultRegularThroughOther;
	}

	public void setExamResultRegularThroughOther(Boolean examResultRegularThroughOther) {
		this.examResultRegularThroughOther = examResultRegularThroughOther;
	}

	public Boolean isTeachingStaff() {
		return teachingStaff;
	}

	public void setTeachingStaff(Boolean teachingStaff) {
		this.teachingStaff = teachingStaff;
	}

	public Boolean isTeachingStaffEcontact() {
		return teachingStaffEcontact;
	}

	public void setTeachingStaffEcontact(Boolean teachingStaffEcontact) {
		this.teachingStaffEcontact = teachingStaffEcontact;
	}

	public Boolean isTeachingStaffVernacularName() {
		return teachingStaffVernacularName;
	}

	public void setTeachingStaffVernacularName(Boolean teachingStaffVernacularName) {
		this.teachingStaffVernacularName = teachingStaffVernacularName;
	}

	public Boolean isNonTeachingStaffDetail() {
		return nonTeachingStaffDetail;
	}

	public void setNonTeachingStaffDetail(Boolean nonTeachingStaffDetail) {
		this.nonTeachingStaffDetail = nonTeachingStaffDetail;
	}

	public Boolean isFinancialInfoIncome() {
		return financialInfoIncome;
	}

	public void setFinancialInfoIncome(Boolean financialInfoIncome) {
		this.financialInfoIncome = financialInfoIncome;
	}

	public Boolean isFinancialInfoExpenditure() {
		return financialInfoExpenditure;
	}

	public void setFinancialInfoExpenditure(Boolean financialInfoExpenditure) {
		this.financialInfoExpenditure = financialInfoExpenditure;
	}

	public Boolean isInfra() {
		return infra;
	}

	public void setInfra(Boolean infra) {
		this.infra = infra;
	}

	public Boolean isScholarship() {
		return scholarship;
	}

	public void setScholarship(Boolean scholarship) {
		this.scholarship = scholarship;
	}

	public Boolean isFellowship() {
		return fellowship;
	}

	public void setFellowship(Boolean fellowship) {
		this.fellowship = fellowship;
	}

	public Boolean isEducationLoan() {
		return educationLoan;
	}

	public void setEducationLoan(Boolean educationLoan) {
		this.educationLoan = educationLoan;
	}

	public Boolean isAccreditation() {
		return accreditation;
	}

	public void setAccreditation(Boolean accreditation) {
		this.accreditation = accreditation;
	}

	public Boolean isFinalLock() {
		return finalLock;
	}

	public void setFinalLock(Boolean finalLock) {
		this.finalLock = finalLock;
	}

	@Override
	public String toString() {
		return "CollegeLockStatus [Id=" + Id + ", aisheCode=" + aisheCode + ", surveyYear=" + surveyYear
				+ ", basicDetail=" + basicDetail + ", address=" + address + ", econtact=" + econtact
				+ ", vernacularName=" + vernacularName + ", alternateName=" + alternateName + ", listOfDepartment="
				+ listOfDepartment + ", regularProgOther=" + regularProgOther + ", enrollRegularProgOther="
				+ enrollRegularProgOther + ", enrollRegularForeignStudentThroughOther="
				+ enrollRegularForeignStudentThroughOther + ", examResultRegularThroughOther="
				+ examResultRegularThroughOther + ", teachingStaff=" + teachingStaff + ", teachingStaffEcontact="
				+ teachingStaffEcontact + ", teachingStaffVernacularName=" + teachingStaffVernacularName
				+ ", nonTeachingStaffDetail=" + nonTeachingStaffDetail + ", financialInfoIncome=" + financialInfoIncome
				+ ", financialInfoExpenditure=" + financialInfoExpenditure + ", infra=" + infra + ", scholarship="
				+ scholarship + ", fellowship=" + fellowship + ", educationLoan=" + educationLoan + ", accreditation="
				+ accreditation + ", finalLock=" + finalLock + "]";
	}

	public CollegeLockStatus() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CollegeLockStatus(int id, String aisheCode, int surveyYear, Boolean basicDetail, Boolean address,
			Boolean econtact, Boolean vernacularName, Boolean alternateName, Boolean listOfDepartment,
			Boolean regularProgOther, Boolean enrollRegularProgOther, Boolean enrollRegularForeignStudentThroughOther,
			Boolean examResultRegularThroughOther, Boolean teachingStaff, Boolean teachingStaffEcontact,
			Boolean teachingStaffVernacularName, Boolean nonTeachingStaffDetail, Boolean financialInfoIncome,
			Boolean financialInfoExpenditure, Boolean infra, Boolean scholarship, Boolean fellowship,
			Boolean educationLoan, Boolean accreditation, Boolean finalLock) {
		super();
		Id = id;
		this.aisheCode = aisheCode;
		this.surveyYear = surveyYear;
		this.basicDetail = basicDetail;
		this.address = address;
		this.econtact = econtact;
		this.vernacularName = vernacularName;
		this.alternateName = alternateName;
		this.listOfDepartment = listOfDepartment;
		this.regularProgOther = regularProgOther;
		this.enrollRegularProgOther = enrollRegularProgOther;
		this.enrollRegularForeignStudentThroughOther = enrollRegularForeignStudentThroughOther;
		this.examResultRegularThroughOther = examResultRegularThroughOther;
		this.teachingStaff = teachingStaff;
		this.teachingStaffEcontact = teachingStaffEcontact;
		this.teachingStaffVernacularName = teachingStaffVernacularName;
		this.nonTeachingStaffDetail = nonTeachingStaffDetail;
		this.financialInfoIncome = financialInfoIncome;
		this.financialInfoExpenditure = financialInfoExpenditure;
		this.infra = infra;
		this.scholarship = scholarship;
		this.fellowship = fellowship;
		this.educationLoan = educationLoan;
		this.accreditation = accreditation;
		this.finalLock = finalLock;
	}
	
	
	
}

