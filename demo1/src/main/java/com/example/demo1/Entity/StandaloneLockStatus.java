package com.example.demo1.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="standalone_lock_status")
public class StandaloneLockStatus {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int Id;
	
	@Column(name="aishe_code")
	private String aisheCode;
	
	@Column(name="survey_year")
	private int surveyYear;
	
	@Column(name="basic_detail")
	private Boolean basicDetail;
	
	@Column(name="address")
	private Boolean address;
	
	@Column(name="econtact")
	private Boolean econtact;
	
	@Column(name="vernacular_name")
	private Boolean vernacularName;
	
	@Column(name="alternate_name")
	private Boolean alternateName;
	
	@Column(name="list_of_department")
	private Boolean listOfDepartment;
	
	@Column(name="regular_prog_through_department")
	private Boolean regularProgThroughDepartment;

	@Column(name="regular_prog_other")
	private Boolean regularProgOther;
	
	@Column(name="distance_prog_through_department")
	private Boolean distanceProgThroughDepartment;

	@Column(name="distance_prog_other")
	private Boolean distanceProgOther;
	
	@Column(name="enroll_regular_prog_through_department")
	private Boolean enrollRegularProgThroughDepartment;

	@Column(name="enroll_regular_prog_other")
	private Boolean enrollRegularProgOther;
	
	@Column(name="enroll_distance_prog_through_department")
	private Boolean enrollDistanceProgThroughDepartment;

	@Column(name="enroll_distance_prog_other")
	private Boolean enrollDistanceProgOther;
	
	@Column(name="enroll_regular_foreign_student_through_department")
	private Boolean enrollRegularForeignStudentThroughDepartment;

	@Column(name="enroll_regular_foreign_student_through_other")
	private Boolean enrollRegularForeignStudentThroughOther;
	
	@Column(name="enroll_distance_foreign_student_through_department")
	private Boolean enrollDistanceForeignStudentThroughDepartment;

	@Column(name="enroll_distance_foreign_student_through_other")
	private Boolean enrollDistanceForeignStudentThroughOther;
	
	@Column(name="enroll_foreign_student_count")
	private Boolean enrollForeignStudentCount;

	@Column(name="exam_result_regular_through_department")
	private Boolean examResultRegularThroughDepartment;
	
	@Column(name="exam_result_regular_through_other")
	private Boolean examResultRegularThroughOther;
	
	@Column(name="exam_result_distance_through_department")
	private Boolean examResultDistanceThroughDepartment;
	
	@Column(name="exam_result_distance_through_other")
	private Boolean examResultDistanceThroughOther;
	
	@Column(name="teaching_staff")
	private Boolean teachingStaff;
	
	@Column(name="teaching_staff_econtact")
	private Boolean teachingStaffEcontact;
	
	@Column(name="teaching_staff_vernacular_name")
	private Boolean teachingStaffVernacularName;
	
	@Column(name="non_teaching_staff_detail")
	private Boolean nonTeachingStaffDetail;
	
	@Column(name="financial_info_income")
	private Boolean financialInfoIncome;
	
	@Column(name="financial_info_expenditure")
	private Boolean financialInfoExpenditure;
	
	@Column(name="infra")
	private Boolean infra;
	
	@Column(name="scholarship")
	private Boolean scholarship;
	
	@Column(name="fellowship")
	private Boolean fellowship;
	
	@Column(name="education_loan")
	private Boolean educationLoan;
	
	@Column(name="accreditation")
	private Boolean accreditation;
	
	@Column(name="final_lock")
	private Boolean finalLock;

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getAisheCode() {
		return aisheCode;
	}

	public void setAisheCode(String aisheCode) {
		this.aisheCode = aisheCode;
	}

	public int getSurveyYear() {
		return surveyYear;
	}

	public void setSurveyYear(int surveyYear) {
		this.surveyYear = surveyYear;
	}

	public Boolean isBasicDetail() {
		return basicDetail;
	}

	public void setBasicDetail(Boolean basicDetail) {
		this.basicDetail = basicDetail;
	}

	public Boolean isAddress() {
		return address;
	}

	public void setAddress(Boolean address) {
		this.address = address;
	}

	public Boolean isEcontact() {
		return econtact;
	}

	public void setEcontact(Boolean econtact) {
		this.econtact = econtact;
	}

	public Boolean isVernacularName() {
		return vernacularName;
	}

	public void setVernacularName(Boolean vernacularName) {
		this.vernacularName = vernacularName;
	}

	public Boolean isAlternateName() {
		return alternateName;
	}

	public void setAlternateName(Boolean alternateName) {
		this.alternateName = alternateName;
	}

	public Boolean isListOfDepartment() {
		return listOfDepartment;
	}

	public void setListOfDepartment(Boolean listOfDepartment) {
		this.listOfDepartment = listOfDepartment;
	}

	public Boolean isRegularProgThroughDepartment() {
		return regularProgThroughDepartment;
	}

	public void setRegularProgThroughDepartment(Boolean regularProgThroughDepartment) {
		this.regularProgThroughDepartment = regularProgThroughDepartment;
	}

	public Boolean isRegularProgOther() {
		return regularProgOther;
	}

	public void setRegularProgOther(Boolean regularProgOther) {
		this.regularProgOther = regularProgOther;
	}

	public Boolean isDistanceProgThroughDepartment() {
		return distanceProgThroughDepartment;
	}

	public void setDistanceProgThroughDepartment(Boolean distanceProgThroughDepartment) {
		this.distanceProgThroughDepartment = distanceProgThroughDepartment;
	}

	public Boolean isDistanceProgOther() {
		return distanceProgOther;
	}

	public void setDistanceProgOther(Boolean distanceProgOther) {
		this.distanceProgOther = distanceProgOther;
	}

	public Boolean isEnrollRegularProgThroughDepartment() {
		return enrollRegularProgThroughDepartment;
	}

	public void setEnrollRegularProgThroughDepartment(Boolean enrollRegularProgThroughDepartment) {
		this.enrollRegularProgThroughDepartment = enrollRegularProgThroughDepartment;
	}

	public Boolean isEnrollRegularProgOther() {
		return enrollRegularProgOther;
	}

	public void setEnrollRegularProgOther(Boolean enrollRegularProgOther) {
		this.enrollRegularProgOther = enrollRegularProgOther;
	}

	public Boolean isEnrollDistanceProgThroughDepartment() {
		return enrollDistanceProgThroughDepartment;
	}

	public void setEnrollDistanceProgThroughDepartment(Boolean enrollDistanceProgThroughDepartment) {
		this.enrollDistanceProgThroughDepartment = enrollDistanceProgThroughDepartment;
	}

	public Boolean isEnrollDistanceProgOther() {
		return enrollDistanceProgOther;
	}

	public void setEnrollDistanceProgOther(Boolean enrollDistanceProgOther) {
		this.enrollDistanceProgOther = enrollDistanceProgOther;
	}

	public Boolean isEnrollRegularForeignStudentThroughDepartment() {
		return enrollRegularForeignStudentThroughDepartment;
	}

	public void setEnrollRegularForeignStudentThroughDepartment(Boolean enrollRegularForeignStudentThroughDepartment) {
		this.enrollRegularForeignStudentThroughDepartment = enrollRegularForeignStudentThroughDepartment;
	}

	public Boolean isEnrollRegularForeignStudentThroughOther() {
		return enrollRegularForeignStudentThroughOther;
	}

	public void setEnrollRegularForeignStudentThroughOther(Boolean enrollRegularForeignStudentThroughOther) {
		this.enrollRegularForeignStudentThroughOther = enrollRegularForeignStudentThroughOther;
	}

	public Boolean isEnrollDistanceForeignStudentThroughDepartment() {
		return enrollDistanceForeignStudentThroughDepartment;
	}

	public void setEnrollDistanceForeignStudentThroughDepartment(Boolean enrollDistanceForeignStudentThroughDepartment) {
		this.enrollDistanceForeignStudentThroughDepartment = enrollDistanceForeignStudentThroughDepartment;
	}

	public Boolean isEnrollDistanceForeignStudentThroughOther() {
		return enrollDistanceForeignStudentThroughOther;
	}

	public void setEnrollDistanceForeignStudentThroughOther(Boolean enrollDistanceForeignStudentThroughOther) {
		this.enrollDistanceForeignStudentThroughOther = enrollDistanceForeignStudentThroughOther;
	}

	public Boolean isEnrollForeignStudentCount() {
		return enrollForeignStudentCount;
	}

	public void setEnrollForeignStudentCount(Boolean enrollForeignStudentCount) {
		this.enrollForeignStudentCount = enrollForeignStudentCount;
	}

	public Boolean isExamResultRegularThroughDepartment() {
		return examResultRegularThroughDepartment;
	}

	public void setExamResultRegularThroughDepartment(Boolean examResultRegularThroughDepartment) {
		this.examResultRegularThroughDepartment = examResultRegularThroughDepartment;
	}

	public Boolean isExamResultRegularThroughOther() {
		return examResultRegularThroughOther;
	}

	public void setExamResultRegularThroughOther(Boolean examResultRegularThroughOther) {
		this.examResultRegularThroughOther = examResultRegularThroughOther;
	}

	public Boolean isExamResultDistanceThroughDepartment() {
		return examResultDistanceThroughDepartment;
	}

	public void setExamResultDistanceThroughDepartment(Boolean examResultDistanceThroughDepartment) {
		this.examResultDistanceThroughDepartment = examResultDistanceThroughDepartment;
	}

	public Boolean isExamResultDistanceThroughOther() {
		return examResultDistanceThroughOther;
	}

	public void setExamResultDistanceThroughOther(Boolean examResultDistanceThroughOther) {
		this.examResultDistanceThroughOther = examResultDistanceThroughOther;
	}

	public Boolean isTeachingStaff() {
		return teachingStaff;
	}

	public void setTeachingStaff(Boolean teachingStaff) {
		this.teachingStaff = teachingStaff;
	}

	public Boolean isTeachingStaffEcontact() {
		return teachingStaffEcontact;
	}

	public void setTeachingStaffEcontact(Boolean teachingStaffEcontact) {
		this.teachingStaffEcontact = teachingStaffEcontact;
	}

	public Boolean isTeachingStaffVernacularName() {
		return teachingStaffVernacularName;
	}

	public void setTeachingStaffVernacularName(Boolean teachingStaffVernacularName) {
		this.teachingStaffVernacularName = teachingStaffVernacularName;
	}

	public Boolean isNonTeachingStaffDetail() {
		return nonTeachingStaffDetail;
	}

	public void setNonTeachingStaffDetail(Boolean nonTeachingStaffDetail) {
		this.nonTeachingStaffDetail = nonTeachingStaffDetail;
	}

	public Boolean isFinancialInfoIncome() {
		return financialInfoIncome;
	}

	public void setFinancialInfoIncome(Boolean financialInfoIncome) {
		this.financialInfoIncome = financialInfoIncome;
	}

	public Boolean isFinancialInfoExpenditure() {
		return financialInfoExpenditure;
	}

	public void setFinancialInfoExpenditure(Boolean financialInfoExpenditure) {
		this.financialInfoExpenditure = financialInfoExpenditure;
	}

	public Boolean isInfra() {
		return infra;
	}

	public void setInfra(Boolean infra) {
		this.infra = infra;
	}

	public Boolean isScholarship() {
		return scholarship;
	}

	public void setScholarship(Boolean scholarship) {
		this.scholarship = scholarship;
	}

	public Boolean isFellowship() {
		return fellowship;
	}

	public void setFellowship(Boolean fellowship) {
		this.fellowship = fellowship;
	}

	public Boolean isEducationLoan() {
		return educationLoan;
	}

	public void setEducationLoan(Boolean educationLoan) {
		this.educationLoan = educationLoan;
	}

	public Boolean isAccreditation() {
		return accreditation;
	}

	public void setAccreditation(Boolean accreditation) {
		this.accreditation = accreditation;
	}

	public Boolean isFinalLock() {
		return finalLock;
	}

	public void setFinalLock(Boolean finalLock) {
		this.finalLock = finalLock;
	}

	@Override
	public String toString() {
		return "StandaloneLockStatus [Id=" + Id + ", aisheCode=" + aisheCode + ", surveyYear=" + surveyYear
				+ ", basicDetail=" + basicDetail + ", address=" + address + ", econtact=" + econtact
				+ ", vernacularName=" + vernacularName + ", alternateName=" + alternateName + ", listOfDepartment="
				+ listOfDepartment + ", regularProgThroughDepartment=" + regularProgThroughDepartment
				+ ", regularProgOther=" + regularProgOther + ", distanceProgThroughDepartment="
				+ distanceProgThroughDepartment + ", distanceProgOther=" + distanceProgOther
				+ ", enrollRegularProgThroughDepartment=" + enrollRegularProgThroughDepartment
				+ ", enrollRegularProgOther=" + enrollRegularProgOther + ", enrollDistanceProgThroughDepartment="
				+ enrollDistanceProgThroughDepartment + ", enrollDistanceProgOther=" + enrollDistanceProgOther
				+ ", enrollRegularForeignStudentThroughDepartment=" + enrollRegularForeignStudentThroughDepartment
				+ ", enrollRegularForeignStudentThroughOther=" + enrollRegularForeignStudentThroughOther
				+ ", enrollDistanceForeignStudentThroughDepartment=" + enrollDistanceForeignStudentThroughDepartment
				+ ", enrollDistanceForeignStudentThroughOther=" + enrollDistanceForeignStudentThroughOther
				+ ", enrollForeignStudentCount=" + enrollForeignStudentCount + ", examResultRegularThroughDepartment="
				+ examResultRegularThroughDepartment + ", examResultRegularThroughOther="
				+ examResultRegularThroughOther + ", examResultDistanceThroughDepartment="
				+ examResultDistanceThroughDepartment + ", examResultDistanceThroughOther="
				+ examResultDistanceThroughOther + ", teachingStaff=" + teachingStaff + ", teachingStaffEcontact="
				+ teachingStaffEcontact + ", teachingStaffVernacularName=" + teachingStaffVernacularName
				+ ", nonTeachingStaffDetail=" + nonTeachingStaffDetail + ", financialInfoIncome=" + financialInfoIncome
				+ ", financialInfoExpenditure=" + financialInfoExpenditure + ", infra=" + infra + ", scholarship="
				+ scholarship + ", fellowship=" + fellowship + ", educationLoan=" + educationLoan + ", accreditation="
				+ accreditation + ", finalLock=" + finalLock + "]";
	}

	public StandaloneLockStatus() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StandaloneLockStatus(int id, String aisheCode, int surveyYear, Boolean basicDetail, Boolean address,
			Boolean econtact, Boolean vernacularName, Boolean alternateName, Boolean listOfDepartment,
			Boolean regularProgThroughDepartment, Boolean regularProgOther, Boolean distanceProgThroughDepartment,
			Boolean distanceProgOther, Boolean enrollRegularProgThroughDepartment, Boolean enrollRegularProgOther,
			Boolean enrollDistanceProgThroughDepartment, Boolean enrollDistanceProgOther,
			Boolean enrollRegularForeignStudentThroughDepartment, Boolean enrollRegularForeignStudentThroughOther,
			Boolean enrollDistanceForeignStudentThroughDepartment, Boolean enrollDistanceForeignStudentThroughOther,
			Boolean enrollForeignStudentCount, Boolean examResultRegularThroughDepartment,
			Boolean examResultRegularThroughOther, Boolean examResultDistanceThroughDepartment,
			Boolean examResultDistanceThroughOther, Boolean teachingStaff, Boolean teachingStaffEcontact,
			Boolean teachingStaffVernacularName, Boolean nonTeachingStaffDetail, Boolean financialInfoIncome,
			Boolean financialInfoExpenditure, Boolean infra, Boolean scholarship, Boolean fellowship,
			Boolean educationLoan, Boolean accreditation, Boolean finalLock) {
		super();
		Id = id;
		this.aisheCode = aisheCode;
		this.surveyYear = surveyYear;
		this.basicDetail = basicDetail;
		this.address = address;
		this.econtact = econtact;
		this.vernacularName = vernacularName;
		this.alternateName = alternateName;
		this.listOfDepartment = listOfDepartment;
		this.regularProgThroughDepartment = regularProgThroughDepartment;
		this.regularProgOther = regularProgOther;
		this.distanceProgThroughDepartment = distanceProgThroughDepartment;
		this.distanceProgOther = distanceProgOther;
		this.enrollRegularProgThroughDepartment = enrollRegularProgThroughDepartment;
		this.enrollRegularProgOther = enrollRegularProgOther;
		this.enrollDistanceProgThroughDepartment = enrollDistanceProgThroughDepartment;
		this.enrollDistanceProgOther = enrollDistanceProgOther;
		this.enrollRegularForeignStudentThroughDepartment = enrollRegularForeignStudentThroughDepartment;
		this.enrollRegularForeignStudentThroughOther = enrollRegularForeignStudentThroughOther;
		this.enrollDistanceForeignStudentThroughDepartment = enrollDistanceForeignStudentThroughDepartment;
		this.enrollDistanceForeignStudentThroughOther = enrollDistanceForeignStudentThroughOther;
		this.enrollForeignStudentCount = enrollForeignStudentCount;
		this.examResultRegularThroughDepartment = examResultRegularThroughDepartment;
		this.examResultRegularThroughOther = examResultRegularThroughOther;
		this.examResultDistanceThroughDepartment = examResultDistanceThroughDepartment;
		this.examResultDistanceThroughOther = examResultDistanceThroughOther;
		this.teachingStaff = teachingStaff;
		this.teachingStaffEcontact = teachingStaffEcontact;
		this.teachingStaffVernacularName = teachingStaffVernacularName;
		this.nonTeachingStaffDetail = nonTeachingStaffDetail;
		this.financialInfoIncome = financialInfoIncome;
		this.financialInfoExpenditure = financialInfoExpenditure;
		this.infra = infra;
		this.scholarship = scholarship;
		this.fellowship = fellowship;
		this.educationLoan = educationLoan;
		this.accreditation = accreditation;
		this.finalLock = finalLock;
	}
	
	
}


