package com.example.demo1.service;



import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo1.Entity.CollegeLockStatus;
import com.example.demo1.Entity.StandaloneLockStatus;
import com.example.demo1.Entity.UniversityLockStatus;
import com.example.demo1.bean.LockBean;
import com.example.demo1.repo.CollegeLockStatusRepo;
import com.example.demo1.repo.LockStatusRep;
import com.example.demo1.repo.StandaloneLockStatusRepo;
import com.example.demo1.repo.UniversityLockStatusRepo;

@Service

public class ServiceImpl  {
	@Autowired CollegeLockStatusRepo collegeLockStatusRepo;
	@Autowired StandaloneLockStatusRepo standaloneLockStatusRepo;
	@Autowired UniversityLockStatusRepo universityLockStatusRepo;
	@Autowired LockStatusRep lockStatusRep;
	@Autowired  private EntityManager entityManager;
	
	
	@Transactional
	public int saveStandalone(LockBean lockBean) {
		
		return  	entityManager.createNativeQuery("INSERT INTO standalone_lock_status (aishe_code, survey_year, "+lockBean.getConstant()+" ) VALUES (?,?,?)")
			      .setParameter(1, lockBean.getAisheCode())
			      .setParameter(2, lockBean.getSurveyYear())
			      .setParameter(3,lockBean.isStatus())
			      .executeUpdate();
	}
	@Transactional
	public int updateStandalone(LockBean lockBean, StandaloneLockStatus entity) {
		return  entityManager.createNativeQuery("update standalone_lock_status set "+lockBean.getConstant()+" = :Status where aishe_code = :AisheCode and survey_year = :SurveyYear")
				.setParameter("Status", lockBean.isStatus())  
				.setParameter("AisheCode", entity.getAisheCode())
				.setParameter("SurveyYear", entity.getSurveyYear())
			      .executeUpdate();
	}
	
	@Transactional
	public int saveCollege(LockBean lockBean) {
		
		return  	entityManager.createNativeQuery("INSERT INTO college_lock_status (aishe_code, survey_year, "+lockBean.getConstant()+" ) VALUES (?,?,?)")
			      .setParameter(1, lockBean.getAisheCode())
			      .setParameter(2, lockBean.getSurveyYear())
			      .setParameter(3,lockBean.isStatus())
			      .executeUpdate();
	}
	@Transactional
	public int updateCollege(LockBean lockBean, CollegeLockStatus entity) {
		return  entityManager.createNativeQuery("update college_lock_status set "+lockBean.getConstant()+" = :Status where aishe_code = :AisheCode and survey_year = :SurveyYear")
				.setParameter("Status", lockBean.isStatus())  
				.setParameter("AisheCode", entity.getAisheCode())
				.setParameter("SurveyYear", entity.getSurveyYear())
			      .executeUpdate();
	}
	
	@Transactional
	public int saveUniversity(LockBean lockBean) {
		
		return  	entityManager.createNativeQuery("INSERT INTO university_lock_status (aishe_code, survey_year, "+lockBean.getConstant()+" ) VALUES (?,?,?)")
			      .setParameter(1, lockBean.getAisheCode())
			      .setParameter(2, lockBean.getSurveyYear())
			      .setParameter(3,lockBean.isStatus())
			      .executeUpdate();
	}
	@Transactional
	public int updateUniversity(LockBean lockBean, UniversityLockStatus entity) {
		return  entityManager.createNativeQuery("update university_lock_status set "+lockBean.getConstant()+" = :Status where aishe_code = :AisheCode and survey_year = :SurveyYear")
				.setParameter("Status", lockBean.isStatus())  
				.setParameter("AisheCode", entity.getAisheCode())
				.setParameter("SurveyYear", entity.getSurveyYear())
			      .executeUpdate();
	}

}
