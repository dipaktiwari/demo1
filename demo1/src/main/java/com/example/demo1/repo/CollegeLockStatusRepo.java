package com.example.demo1.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo1.Entity.CollegeLockStatus;
import com.example.demo1.Entity.StandaloneLockStatus;

@Repository
public interface CollegeLockStatusRepo extends JpaRepository<CollegeLockStatus, Integer> {
	
	public CollegeLockStatus findByAisheCodeAndSurveyYear(String aisheCode, int surveyYear);

}
