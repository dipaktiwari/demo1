package com.example.demo1.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo1.Entity.CollegeLockStatus;
import com.example.demo1.Entity.UniversityLockStatus;

@Repository

public interface UniversityLockStatusRepo extends JpaRepository<UniversityLockStatus, Integer> {
	
	public UniversityLockStatus findByAisheCodeAndSurveyYear(String aisheCode, int surveyYear);


}
