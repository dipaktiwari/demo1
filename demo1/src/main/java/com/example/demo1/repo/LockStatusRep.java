package com.example.demo1.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo1.Entity.LockStatus;
@Repository
public interface LockStatusRep extends JpaRepository<LockStatus, Long> {

}
