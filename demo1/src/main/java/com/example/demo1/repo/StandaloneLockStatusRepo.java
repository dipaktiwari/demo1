package com.example.demo1.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo1.Entity.StandaloneLockStatus;

@Repository
public interface StandaloneLockStatusRepo extends JpaRepository<StandaloneLockStatus, Integer> {

	
	public StandaloneLockStatus findByAisheCodeAndSurveyYear(String aisheCode, int surveyYear);
}
