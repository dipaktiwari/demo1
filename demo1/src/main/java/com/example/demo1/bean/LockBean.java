package com.example.demo1.bean;


 
public class LockBean {

	private String intitutionType;
	private String aisheCode;
	private int surveyYear;
	private boolean status;
	private String constant;
	
	public String getIntitutionType() {
		return intitutionType;
	}
	public void setIntitutionType(String intitutionType) {
		this.intitutionType = intitutionType;
	}
	public String getAisheCode() {
		return aisheCode;
	}
	public void setAisheCode(String aisheCode) {
		this.aisheCode = aisheCode;
	}
	public int getSurveyYear() {
		return surveyYear;
	}
	public void setSurveyYear(int surveyYear) {
		this.surveyYear = surveyYear;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getConstant() {
		return constant;
	}
	public void setConstant(String constant) {
		this.constant = constant;
	}
	@Override
	public String toString() {
		return "LockBean [IntitutionType=" + intitutionType + ", aisheCode=" + aisheCode + ", surveyYear=" + surveyYear
				+ ", status=" + status + ", constant=" + constant + "]";
	}
	public LockBean(String intitutionType, String aisheCode, int surveyYear, boolean status, String constant) {
		super();
		intitutionType = intitutionType;
		this.aisheCode = aisheCode;
		this.surveyYear = surveyYear;
		this.status = status;
		this.constant = constant;
	}
	public LockBean() {
		super();
		// TODO Auto-generated constructor stub
	}
}
